// 1. Create an "activity" folder, an "index.html" file inside of it and link the "index.js" file.

// 2. Create an "index.js" file and console log the message "Hello World" to ensure that the script file is properly associated with the html file.

// 3. Create a function that will accept the first name, last name and age as arguments and print those details in the console as a single string.

// 4. Create another function that will return a value and store it in a variable.

console.log('Hello World')


function fullName(){
   //combine the values
   console.log(givenName + ' ' + familyName + ' ' + 'is' + ' ' + age + ' ' + 'years of age');
}
let givenName = 'John'; 
let familyName = 'Smith';
let age = '30';

//Invoking/Calling out functions
fullName();


function dialog(){
	return console.log('This was printed inside of the function');
	
}

//invocation
console.log(dialog());

function dialog2(Married) {
  console.log('The value of isMarried is: ' + Married);
}

dialog2(true);

// Instructor's Solution

// console.log("Hello World");

//we may different approaches as long as the logic is there then it will be marked as passed.

// function printUserInfo(firstName, lastName, age){
   // console.log(firstName + ' ' + lastName + ' is ' + age + ' years of age.');
// }

//call out the function
// printUserInfo("John", "Smith", "30");

//create a function that will return a value
// function returnFunction() {
	// console.log('This was printed out inside a function'); //optional
	// return true; //answers a question of yes/No
	// console.log('This was printed out inside a function'); //ignored
// }

// let isMarried = returnFunction(); 
// console.log("The value of isMarried is: " + isMarried ); //optional
